# Projet PycMan

Implementation d'un PacMan avec Python3 et Pygame

## Fonctionalites :

PycMan se deplace dans un labyrinthe
Il peux warper d'un bout a l'autre du niveau a l'aide de tunnel
En mangeant des bonus il augmente son score
4 bonus par niveau rendent les fantomes vulnerables
Partie Perdu si attrape par un fantome
Systeme avec different niveau

## Prevision :

15 min preparation papier
15 min preparation PC

28 x 25 min pour preparer le prototype
28 x 25 min pour peaufiner

Construction d'une feuille de route pour suivre le projet

## Idees :

Ajouter des images pour rendre le jeu plus attrayant
Faire une suite de niveau interessant
Coder different comportement pour les fantomes (embuscade, aleatoire, agressif, warp autorise)
Avoir des animation de deplacement fluide
Transition en case par case
Ajout d'effet (particule, lumiere...)

