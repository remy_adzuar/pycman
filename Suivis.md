# Suivis du projet

1 ere session
25 min, Reflexion sur l'architecture a employer
5 min pause,
25 min, Debut implementation des classes, premiers test de labyrinthe
5 min pause,
25 min, Mise en place des premiers dessins pour labyrinthe et pour le joueur
5 min pause,
25 min, Colision entre le joueur et les murs
Fin de session

2 eme session
25 min, Amélioration déplacement du joueur, Début implementation des fantômes
(oui je viens de retrouver les accents sur mon clavier qwerty...)
5 min pause,
25 min, Déplacement basiques des fantômes ok
5 min pause,
25 min, Refacto déplacement fantômes dans une classe Ia_move, ajout calcul de distance de manhattan pour gérer les collisions
5 min pause,
25 min, Début implémentation Bonus, Fonction dans Labyrinthe pour retourner des listes de position de floor
Fin de session

3 eme session
25 min, Ajout Bonus mangeable et condition de victoire / défaite
5 min pause, 
25 min, Ajout super bonus, Ajout classe Timer
5 min pause,
25 min, Ajout mécanisme pour manger les fantômes
5 min pause,
25 min, Mise en place lecture de fichier
fin de session

4 eme session
25 min, Reproduction du niveau de pacman original, réflexion sur Déplacements
5 min pause,
25 min, Début implémentation du déplacement doux et non plus en case par case
5 min pause,
25 min, Smooth move ok pour le joueur, En cours pour les fantômes
5 min pause,
25 min, Smooth move ok pour les fantômes, début classe text
Fin de session

5 eme session
25 min, Ajout de select text, debut de l'ajout d'interface
5 min pause,
25 min, Ajout menu, quitter, préparation algo de recherche de chemin pour le comportement IA
plus élaboré
Fin de session

6 eme session
25 min, Amélioration du déplacement des fantômes, début de la classe sprite perso
5 min pause,
25 min, Rotation de l'animation en sprite, test plutôt concluant
Fin de session

30/12/2022
7 eme session
25 min, Amélioration classe sprite
5 min pause,
25 min, Travail sur les rotations de sprite
Fin de session

8 eme session
25 min, Ajout animation PycMan en jeu
5 min pause,
25 min, Ajout animation fantômes, Bug graphique quant fantômes mangé
5 min pause,
25 min, Résolution Bug Graphique, Début murs PycMan
5 min pause,
25 min, Début implémentation des images de murs
Fin de session

## Observation
Je suis claqué!

9 eme session
25 min, Début fonction pour déterminer le type de sprite pour les murs
5 min pause,
25 min, Affichage des murs selon leur nombre de voisin
Fin de session

Fin de prototypage

## Observation

Je suis à la moitié de mon budget temps, j'ai un prototype de jeu qui fonctionne dans ses grande règles,
il manque en terme de logique de faire une chambre centrale pour les fantômes avec un murs uniquement traversable
par ces derniers, et de rajouter le systeme de warp entre les deux points de la map.

Il manque absolument tout le HUD et la logique afférente (systeme de score, de vie, de niveau)
et il reste du travail sur l'affichage des murs

À priori, sauf grosses difficultés imprévu, l'ensemble du peaufinage devrait tenir dans le budget temps prévu

## Peaufinage

1 ere session
25 min, Orientation des murs,
5 min pause,
25 min, Orientation Mur OK, Mort de Pycman + ajout nombre de vie
5 min pause,
25 min, Ajout Mort de PycMan, ajout fantôme de remplacement
5 min pause,
25 min, Ajout du mur fantômes, modif position de départ des fantômes
Fin de session

2eme session
25 min, Ajout fonctionnement des murs fantômes
5 min pause,
25 min, Réflexion recherche de chemin
Fin de session

3 eme session
25 min, Début implémentation Algorithme de recherche
5 min pause,
25 min, Algorithme A* en Cours

## Observation

Il va surement falloir faire une pause d'un jour sur le projet, je fatigue,
motivation au plus bas

02/01/2023

4 eme session
25 min, résolution algorithme de chemin
5 min pause,
25 min, Ajout algo chemin pour la disposition des bonus
10 min pause,
25 min, Ajout de Text pour les bonus mangés
5 min pause
25 min, Ajout affichage de la vie, du score et du nombre de bonus restant a manger
Fin de session
25 min, ajout téléportation pycman
Fin de session

03/01/2023
5 eme session
25 min, Ajout classe de score, ajout crédits
5 min pause,
25 min, Ajout tableau de score, Ajout classe input text
Fin de session
25 min, implémentation ajout du score
5 min pause,
25 min, Bug implémentation ajout du score
Fin de session