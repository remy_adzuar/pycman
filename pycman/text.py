import pygame

from config import Config


class Text:

    def __init__(self,config:Config, pos:list, content:str="", center:bool=False) -> None:
       self.config = config
       self.pos = pos
       self.content = content
       self.center = center

       self.tx_color = (255,255,255)
       self.bg_color = (0,0,0)
       self.load_text()
    
    def set_tx_color(self, color:list):
        self.tx_color = color
        self.load_text()

    def set_bg_color(self, color:list):
        self.bx_color = color
        self.load_text()
    
    def load_text(self):
        font = self.config.get_font()
        rendu = font.render(self.content, True, self.tx_color, self.bg_color)
        self.rect = rendu.get_rect()
        if self.center:
            self.rect.center = self.pos
        else:
            self.rect.topleft = self.pos
        self.rendu = rendu
    
    def print_text(self):
        self.load_text()
        screen = self.config.get_screen()
        screen.blit(self.rendu, self.rect)

class Select_Text(Text):
    def __init__(self, config: Config, pos: list, target:str, content: str = "", center: bool = False) -> None:
        self.selected = False
        self.target = target
        self.tx_select_color = (0,0,0)
        self.bg_select_color = (255,255,255)
        super().__init__(config, pos, content, center)

    def load_text(self):
        font = self.config.get_font()
        mouse_pos = pygame.mouse.get_pos()

        if hasattr(self, "rect") and self.rect.collidepoint(mouse_pos):
            rendu = font.render(self.content, True, self.tx_select_color, self.bg_select_color)
        else:
            rendu = font.render(self.content, True, self.tx_color, self.bg_color)
        
        self.rect = rendu.get_rect()
        if self.center:
            self.rect.center = self.pos
        else:
            self.rect.topleft = self.pos
        self.rendu = rendu
    
    def print_text(self):
        self.load_text()
        super().print_text()
    
    def on_click(self, ev:pygame.event):
            mouse_pos = pygame.mouse.get_pos()
            if self.rect.collidepoint(mouse_pos):
                if ev.button == 1:
                    return self.target

class Input_Text(Text):
    def __init__(self, config: Config, pos: list, content:str="", center: bool = False) -> None:
        super().__init__(config, pos, content, center)
        self._input = ""
    
    def add_char(self, char):
        self._input += char
        self.load_text(self.content+self._input)
    
    def remove_char(self):
        self._input = self._input[:-1]
    
    def get_content(self):
        return self.content
    
    def get_input(self):
        return self._input

class Fading_Text(Text):
    # Class with text that fading in time, and finally disapear
    # will it use for print score on screen when pycman eating up bonus or ghost
    
    def __init__(self, config: Config, pos: list, content: str = "", center: bool = False) -> None:
        super().__init__(config, pos, content, center)
        self.tx_color = (0,255,64)
        self.frame = 0
        # self.alpha = 255
        self.state = "ALIVE"

    def load_text(self, alpha=255):
        font = self.config.get_font()
        rendu = font.render(self.content, True, self.tx_color)
        rendu.set_alpha(alpha)
        self.rect = rendu.get_rect()
        if self.center:
            self.rect.center = self.pos
        else:
            self.rect.topleft = self.pos
        self.rendu = rendu
    
    def update(self):
        self.frame += 1


        if self.frame%4 == 0:
            self.pos = (self.pos[0], self.pos[1]-2)
            # self.alpha -= 4
            # if self.alpha <= 0:
            #     self.alpha = 0 
            self.load_text()

        if self.frame == 60:
            self.content = ""
            self.state = "DEAD"
            self.load_text()


class Text_list:
    def __init__(self) -> None:
        self.texts = []
    
    def append_text(self, text:Text):
        self.texts.append(text)
    
    def print_texts(self):
        for text in self.texts:
            if isinstance(text, Text):
                text.print_text()
    
    def on_click(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            for text in self.texts:
                if isinstance(text, Select_Text):
                    res = text.on_click(event)
                    if isinstance(res, str):
                        return res
