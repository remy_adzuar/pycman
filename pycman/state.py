import pygame

from game import Game

from config import Config
from text import Text, Select_Text, Text_list, Input_Text
from score import Score

class State:

    def __init__(self) -> None:
        self.state = "MENU"
        self.config = Config()
        self._score = Score()
        self._score.read_file_score("./scores.txt")

    def menu(self):

        txt_ls = Text_list()
        txt_ls.append_text(Select_Text(self.config, (128,128),"GAME", "# Nouveau Jeu #"))
        txt_ls.append_text(Select_Text(self.config, (128,160),"SCORE", "# Tableau de Score #"))
        txt_ls.append_text(Select_Text(self.config, (128,192),"CREDITS", "# Crédits #"))
        txt_ls.append_text(Select_Text(self.config, (128,224),"QUIT", "# Quitter #"))
        result = None

        self.loop = True
        while self.loop:

            self.config.get_screen().fill((0,0,0))

            txt_ls.print_texts()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.loop = False
                    self.state = "QUIT"
                    return None
                result = txt_ls.on_click(event)

            if result != None:
                if result == "GAME":
                    self.loop = False
                    self.state = "GAME"
                elif result == "CREDITS":
                    self.loop = False
                    self.state = "CREDITS"
                elif result == "SCORE":
                    self.loop = False
                    self.state  = "SCORE"
                elif result == "QUIT":
                    self.loop = False
                    self.state = "QUIT"

            pygame.display.update()

    def game(self):

        self._game = Game(self.config)
        self._game.run()
        self.state = "SCORE"
    
    def score(self):

        save_score = False
        txt_ls = Text_list()
        txt_ls.append_text(Text(self.config, (128,96), "Tableau de Score : "))
        i = 128
        for score in self._score.get_scores():
            txt = Text(self.config, (128,i), str(score[0])+" : "+str(score[1]))
            txt_ls.append_text(txt)
            i += 32

        if hasattr(self, "_game"):
            if self._game.grid.score > 0:
                # Devra enregistrer le nouveau score
                name = ""
                name_field = Input_Text(self.config, (128, i), "ENTREZ VOTRE NOM >"+name)
                save_score = True

        self.loop = True
        while self.loop:

            self.config.get_screen().fill((0,0,0))

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.loop = False
                    self.state = "QUIT"
                if event.type == pygame.KEYDOWN and save_score:
                    if save_score:
                        if event.key == pygame.K_RETURN:
                            if len(name_field.get_input())>=3:
                                self._score.append_score([name_field.get_input(), self._game.grid.score])
                                self._score.write_score_file("./scores.txt")
                                # txt_ls.append_text(Text(self.config, (128,i), str(name_field.get_input())+" : "+str(self._game.grid.score)))
                                save_score = False
                            else:
                                pass
                        if event.key == pygame.K_BACKSPACE:
                            name_field.remove_char()

                        else:
                            try:
                                name_field.add_char(event.unicode)
                            except:
                                print("PAS UNICODE")
                    elif event.type == pygame.KEYDOWN and not save_score:
                        self.loop = False
                        self.state = "MENU"
            
            txt_ls.print_texts()
            if save_score:
                name_field.print_text()

            pygame.display.update()

        if hasattr(self, "_game"):
            self._game = None
    
    def credits(self):
        txt_ls = Text_list()
        txt_ls.append_text(Text(self.config, (128,128), "PycMan"))
        txt_ls.append_text(Text(self.config, (128,160), "Développé par : ADZUAR Rémy"))

        self.loop = True
        while self.loop:

            self.config.get_screen().fill((0,0,0))

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.loop = False
                    self.state = "QUIT"
                if event.type == pygame.KEYDOWN:
                    self.loop = False
                    self.state = "MENU"
            
            txt_ls.print_texts()

            pygame.display.update()

    ##

    ## Fonction de classe

    ##

    def get_state(self):
        return self.state