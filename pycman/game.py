import pygame

from config import Config
from grid import Grid
from entity import *
from text import *

class Game:

    def __init__(self, config:Config) -> None:
        self.config = config
        self.grid = Grid(self.config)
        self.start = pygame.time.get_ticks()



    def run(self):
        self.load()

        self.loop = True
        while self.loop:
            self.config.get_screen().fill((0,0,0))

            self.update()
            self.draw()
            self.process_key()

            pygame.display.update()
            self.config.get_clock().tick(60)

    def load(self):
        self.grid.load()

    def update(self):
        self.grid.update()
        if self.grid.state == "DEAD":
            self.loop = False
        if self.grid.bonus_count == 0:
            self.loop = False

        self.life_UI = Text(self.config, (900,32), "Vie : "+str(self.grid.player.life))
        self.score_UI = Text(self.config, (900, 64), "Score : "+str(self.grid.score))
        self.bonus_left_UI = Text(self.config, (900, 96), "Bonus Restant : "+str(self.grid.bonus_count))

    def draw(self):
        self.grid.draw()
        self.life_UI.print_text()
        self.score_UI.print_text()
        self.bonus_left_UI.print_text()

    def process_key(self):
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                self.grid.process_key(event.key)