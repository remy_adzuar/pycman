import pygame

from state import State

class Main:

    def __init__(self) -> None:
        self.state = State()
        self.loop = False
    def run(self):

        self.loop = True
        while self.loop:
            if self.state.get_state() == "MENU":
                self.state.menu()
            elif self.state.get_state() == "GAME":
                self.state.game()
            elif self.state.get_state() == "CREDITS":
                self.state.credits()
            elif self.state.get_state() == "SCORE":
                self.state.score()
            elif self.state.get_state() == "QUIT":
                self.loop = False

if __name__ == "__main__":

    pygame.init()

    M = Main()
    M.run()

    pygame.quit()