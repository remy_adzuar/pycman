import pygame

from config import Config
from my_sprite import *

class Entity:

    def __init__(self, pos:list) -> None:
        self.pos = pos
        self.direction = [0,0]
        self.end_pos = self.pos[:] # Copie de list
        self.real_pos = self.real_pos_calc(self.pos)
        self.real_end_pos = self.real_pos_calc(self.end_pos)

    def set_pos(self, pos:list):
        self.pos = pos
    
    def get_pos(self)->list:
        return self.pos

    def smooth_move(self, enable, speed:int=2):
        if self.real_pos[0] == self.real_end_pos[0] and self.real_pos[1] == self.real_end_pos[1]:
            self.pos = self.end_pos
            # On va chercher la prochain end_pos
            change_end_pos = False
            if self.direction == [1,0] and enable["E"]:
                self.end_pos[0] = self.pos[0]+1
                change_end_pos = True
            elif self.direction == [-1,0] and enable["O"]:
                self.end_pos[0] = self.pos[0]-1
                change_end_pos = True
            elif self.direction == [0,-1] and enable["N"]:
                self.end_pos[1] = self.pos[1]-1
                change_end_pos = True
            elif self.direction == [0,1] and enable["S"]:
                self.end_pos[1] = self.pos[1]+1
                change_end_pos = True
            # Test pour téléportation
            elif self.direction == [-1,0] and self.pos == [0,14]:
                self.set_pos([26,14])
                self.real_pos = self.real_pos_calc(self.pos)
                self.end_pos[0] = self.pos[0]
                change_end_pos = True
            elif self.direction == [1,0] and self.pos == [26,14]:
                self.set_pos([0,14])
                self.real_pos = self.real_pos_calc(self.pos)
                self.end_pos[0] = self.pos[0]
                change_end_pos = True

            if change_end_pos:
                self.real_end_pos = self.real_pos_calc(self.end_pos)

        else:
            # L'entier en dur doit etre diviseur de 32, c'est mieux :3
            if self.real_pos[0] > self.real_end_pos[0]:
                self.real_pos[0] -= speed
            elif self.real_pos[0] < self.real_end_pos[0]:
                self.real_pos[0] += speed
            
            if self.real_pos[1] > self.real_end_pos[1]:
                self.real_pos[1] -= speed
            elif self.real_pos[1] < self.real_end_pos[1]:
                self.real_pos[1] += speed
    
    def real_pos_calc(self, pos):
        # retourne une position en pixel pour une position par case en entree
        x = pos[0]*32 # Faire passer via config
        y = pos[1]*32
        return [x,y]

    def set_direction(self, direction:list):
        self.direction = direction

class Player(Entity):

    def __init__(self, pos: list, sprite_group) -> None:
        super().__init__(pos)
        self.life = 3

        self.anim = My_sprite(sprite_group)
        self.anim.add_image("./imgs/player/PycMan-4.png")
        self.anim.add_image("./imgs/player/PycMan-3.png")
        self.anim.add_image("./imgs/player/PycMan-2.png")
        self.anim.add_image("./imgs/player/PycMan-1.png")
    
        self.death_anim = My_sprite(sprite_group)
        self.death_anim.add_image("./imgs/player/pacman_death-1.png")
        self.death_anim.add_image("./imgs/player/pacman_death-2.png")
        self.death_anim.add_image("./imgs/player/pacman_death-3.png")
        self.death_anim.add_image("./imgs/player/pacman_death-4.png")
        self.death_anim.add_image("./imgs/player/pacman_death-5.png")
        self.death_anim.add_image("./imgs/player/pacman_death-6.png")
        self.death_anim.add_image("./imgs/player/pacman_death-7.png")
        self.death_anim.add_image("./imgs/player/pacman_death-8.png")

    def get_life(self):
        return self.life
    
    def update_life(self, delta):
        self.life += delta

    def dead(self):
        # Normalement figer le jeu et jouer animation de mort
        self.update_life(-1)
        self.set_pos([0,14])
        self.direction = [0,0]
        self.end_pos = self.pos[:] # Copie de list
        self.real_pos = self.real_pos_calc(self.pos)
        self.real_end_pos = self.real_pos_calc(self.end_pos)


    def process_key(self, key):
        if key == pygame.K_RIGHT:
            self.anim.action_rotate(0)
            super().set_direction([1,0])
        elif key == pygame.K_LEFT:
            self.anim.action_rotate(2)
            super().set_direction([-1,0])
        elif key == pygame.K_UP:
            self.anim.action_rotate(1)
            super().set_direction([0,-1])
        elif key == pygame.K_DOWN:
            self.anim.action_rotate(3)
            super().set_direction([0,1])
    
    def draw(self, config:Config):
        # size = config.get_tile_size()
        coords = [self.real_pos[0], self.real_pos[1], 32,32]
        # pygame.draw.rect(config.get_screen(), (0,255,128), coords)
        self.anim.set_pos((coords[0],coords[1]))
        self.anim.update()

class Ghost(Entity):

    def __init__(self, pos: list, sprite_group) -> None:
        super().__init__(pos)
        self.predatory = True
        self.color = (200,200,200)
        self.state = "ALIVE"

        self.anim1 = My_sprite(sprite_group)
        self.anim1.add_image("./imgs/ghost/ghost-1.png")
        self.anim1.add_image("./imgs/ghost/ghost-2.png")
        self.anim1.add_image("./imgs/ghost/ghost-3.png")
    
        self.anim2 = My_sprite(sprite_group)
        self.anim2.add_image("./imgs/ghost/blue_ghost-1.png")
        self.anim2.add_image("./imgs/ghost/blue_ghost-2.png")
        self.anim2.add_image("./imgs/ghost/blue_ghost-3.png")

    def move(self, enable):
        super().smooth_move(enable)
    
    def get_behavior(self):
        return self.predatory
    
    def set_behavior(self, val:bool):
        self.predatory = val
        if val:
            self.color = (200,200,200)
        else:
            self.color = (0,0,200)

    def update(self, enable):
        pass
    
    def draw(self, config:Config):
        coords = [self.real_pos[0], self.real_pos[1], 32,32]
        # pygame.draw.rect(config.get_screen(), self.color, coords)
        if self.get_behavior():
            self.anim1.set_pos((coords[0], coords[1]))
            self.anim1.update()
            self.anim2.set_pos((-16,-16))
        else:
            self.anim2.set_pos((coords[0], coords[1]))
            self.anim2.update()
            self.anim1.set_pos((-16,-16))

class Item(Entity):

    def __init__(self, pos: list, bonus:int) -> None:
        super().__init__(pos)
        self.bonus = bonus
        self.super_bonus = 0 # Bonus normal
        self.color = (255,215,0)

    def get_bonus(self):
        bonus = self.bonus
        self.bonus = 0
        return bonus
    
    def set_super_bonus(self):
        self.super_bonus = 1 # Bonus qui rend les fantomes vulnerable
        self.color = (20,20,255)
    
    def get_type_bonus(self):
        return self.super_bonus

    def draw(self, config:Config):
        size = config.get_tile_size()
        coords = [self.pos[0]*size+8, self.pos[1]*size+8, 16,16]
        pygame.draw.rect(config.get_screen(), self.color, coords)

if __name__ == "__main__":

    test_enable = {
        "N":True,
        "S":True,
        "O":False,
        "E":False
    }

    filtered = [k for k,v in test_enable.items() if v]
