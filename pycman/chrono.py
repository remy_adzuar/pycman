import pygame

from config import Config

class Chrono:

    def __init__(self, config:Config, delay:int) -> None:
        # Delay est en milliseconde
        self.config = config
        self.delay = delay
        self.start = pygame.time.get_ticks()
        self.state = "RUN"
    
    def update(self):
        if pygame.time.get_ticks()-self.start>self.delay:
            self.state = "STOP"