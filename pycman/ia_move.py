import random
from entity import *
from labyrinth import Labyrinth

class Ia_move:

    def __init__(self) -> None:
        pass
    
    def move(self):
        move = False
        if self.entity.direction == [1,0] and self.enable["E"]:
            move = True
        elif self.entity.direction == [-1,0] and self.enable["O"]:
            move = True
        elif self.entity.direction == [0,-1] and self.enable["N"]:
            move = True
        elif self.entity.direction == [0,1] and self.enable["S"]:
            move = True
        if move:
            self.entity.smooth_move(self.enable)
        return move # Permet de savoir si la direction pointe sur un mur

    def change_direction(self):
        enable_true_filter = [k for k,v in self.enable.items() if v]
        i = random.randrange(len(enable_true_filter))
        new_direction = enable_true_filter[i]
        if new_direction == "N":
            self.entity.direction = [0,-1]
        elif new_direction == "S":
            self.entity.direction = [0,1]
        elif new_direction == "O":
            self.entity.direction = [-1,0]
        elif new_direction == "E":
            self.entity.direction = [1,0]


    def basic_movement(self,entity, enable:dict):
        # Function with entity and data injection
        self.enable = enable
        self.entity = entity

        possibility = 0
        for direction in self.enable:
            if self.enable[direction]:
                possibility += 1

        res = self.move()
        if not res or possibility > 2:
            self.change_direction()
        self.enable = None
        self.entity = None
    
    def test_trajectory(self, lab:Labyrinth, start_pos, final_pos, ids):
        # Test d'implémentation d'un algo de recherche de chemin entre deux point
        # start_pos : position de départ
        # final_pos : position d'arrivé
        # ids : dictionnaire de donné qui indique les matière traversable

        # Renvoie la liste des position avec la position de départ en indice 0
        # renvoie une liste vide si chemin introuvable

        # Creation d'une matrice clone
        self.matrix = [[None for i in range(lab.width)] for j in range(lab.height)]
        self.lab = lab

        set_open = [start_pos]
        set_close = []
        self.traject = {}
        while len(set_open)>0:
            self.process_node(set_open, set_close)
        
        # set_open est vide
        # set_close contient l'ensemble des position accessible depuis position de départ
        # self.trajet contient pour chaque clé la valeur de la position antérieur
        # Ce qui doit permettre de reconstituer le trajet
        self.get_traject(start_pos, final_pos)


        # Libération des object, c'est pas propre tant pis
        self.matrix = None
        self.lab = None
        self.traject = None

    def process_node(self, set_open, set_close):

        if len(set_open)>0:
            # Récupère le noeud
            node = set_open.pop()
            list_floor = self.lab.get_walls_neighbourg_pos(node,0)
            for floor in list_floor:
                if floor not in set_close and floor not in set_open:
                    set_open.append(floor)
                    self.traject[(floor[0],floor[1])] = (node[0], node[1])
                
            set_close.append(node)
    
    def get_traject(self, start_pos, final_pos):
        start_pos = (start_pos[0], start_pos[1])
        end_pos = (final_pos[0], final_pos[1])    

        if start_pos in self.traject.values():
            if end_pos in self.traject.keys():
                # alors il existe un chemin

                result = [end_pos]
                while result[-1] != start_pos:
                    pos = result[-1]
                    result.append(self.traject[pos])
                # Result contient le chemin de position pour
                # aller depuis la position d'arriver vers la position de départ :-)
        


if __name__ == "__main__":

    pygame.init()

    config = Config()
    lab = Labyrinth(config)
    lab.load_from_file("./levels/1.txt")
    
    IA = Ia_move()
    IA.test_trajectory(lab, [0,14],[6,16],[0])