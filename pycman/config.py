import pygame

class Config:

    def __init__(self) -> None:
        self.screen = pygame.display.set_mode((1280,1080))
        self.clock = pygame.time.Clock()
        self.tile_size = 32
        self.font = pygame.font.Font("freesansbold.ttf", 16)

    def get_screen(self)->pygame.Surface:
        return self.screen
    
    def get_clock(self)->pygame.time.Clock:
        return self.clock
    
    def get_tile_size(self):
        return self.tile_size
    
    def get_font(self):
        return self.font