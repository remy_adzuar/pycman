from labyrinth import Labyrinth, Case
from entity import *
from ia_move import Ia_move
from chrono import Chrono
from config import Config
from text import *

class Grid:

    def __init__(self, config:Config) -> None:
        self.config = config
        self.labyrinth = Labyrinth(self.config)
        # self.labyrinth.random_labyrinth()
        self.labyrinth.load_from_file("./levels/1.txt")

        self.sprite_group = pygame.sprite.Group()

        self.player = Player([0,14], self.sprite_group)

        self.score = 0

        self.ghosts = [
            Ghost([12,13],self.sprite_group),
            Ghost([13,13], self.sprite_group),
            Ghost([14,13], self.sprite_group),
            Ghost([13,14], self.sprite_group)
        ]
        self.ia_move = Ia_move()
        self.bonus_spawn_number = 240
        self.super_bonus_spawn_number = 4
        self.bonus = []
        self.bonus_count = 0
        self.super_bonus_count = 0

        self.chronos = None

        self.state = ""

        self.fading_texts = []

    def load(self):
        for ghost in self.ghosts:
            # Permet de démarrer le mouvement des ghost
            if isinstance(ghost, Ghost):
                ghost.set_direction([1,0])
        

        list_item_pos = self.labyrinth.give_random_accessible_floor_pos(self.bonus_spawn_number)
        for pos in list_item_pos:
            self.bonus.append(Item(pos, 1))
            if self.super_bonus_spawn_number > self.bonus_count:
                self.bonus[-1].set_super_bonus()
                self.super_bonus_count += 1
            self.bonus_count += 1
    
    def draw(self):
        self.labyrinth.draw_labyrinth()
        self.player.draw(self.config)
        for ghost in self.ghosts:
            ghost.draw(self.config)
        for item in self.bonus:
            if isinstance(item, Item):
                item.draw(self.config)
        
        self.sprite_group.draw(self.config.get_screen())
        
        for text in self.fading_texts:
            text.print_text()
    
    def update(self):

        # Déplacement joueur
        p_enable = self.move_enable(self.player)
        self.player.smooth_move(p_enable)
        # Première vérification de colision
        for ghost in self.ghosts:
            self.check_player_ghost_collide(ghost)
        
        self.check_player_bonus_collide()

        # Déplacement fantômes
        for ghost in self.ghosts:
            g_enable = self.move_enable(ghost)

            self.ia_move.basic_movement(ghost, g_enable)
            
            #Seconde verification de colision, necessaire pour eviter
            # Bug de croisement sans colision
            self.check_player_ghost_collide(ghost)
        
        # Housekeeping ghost
        i = len(self.ghosts)-1
        while i >= 0:
            ghost = self.ghosts[i]
            if ghost.state == "DEAD":
                self.score += 200
                self.ghosts.pop(i)
                self.ghosts.append(Ghost([14,13], self.sprite_group))
            i -= 1

        if self.chronos != None and isinstance(self.chronos, Chrono):
            self.chronos.update()
            if self.chronos.state == "STOP":
                for ghost in self.ghosts:
                    ghost.set_behavior(True)
                self.chronos = None
        
        # Parcour des text pour les update et suppression
        i = len(self.fading_texts)-1
        while i >= 0:
            text = self.fading_texts[i]
            if text.state == "DEAD":
                self.fading_texts.pop(i)
            else:
                text.update()
            i -= 1
        

    def process_key(self, ev):
        self.player.process_key(ev)

    ###########################
    ## -*-*-*-*-*-*-*-*-*-*- ##
    ## Fonctions UTILITAIRES ##
    ## *-*-*-*-*-*-*-*-*-*-* ##
    ###########################

    def move_enable(self, entity):
        adjacent = self.labyrinth.get_adjacent(entity.get_pos())
        enable = {
            "N":False, "S":False, "E":False, "O":False
        }
        for (k, c) in adjacent.items():
            if isinstance(c, Case):
                if c.get_id() == 0:
                    enable[k] = True
                # Autorise le fantôme a franchir le mur selon le comportement
                # du fantôme
                if isinstance(entity, Ghost):
                    if entity.get_behavior():
                        if c.get_id() == 2:
                            if k == "N":
                                enable[k] = True
                    if not entity.get_behavior():
                        if c.get_id() == 2:
                            if k == "S":
                                enable[k] = True
            if c == None:
                enable[k] = True
        return enable

    def delta_pos(self, pos1,pos2):
        # Retourne le nombre de "pas" de distance entre deux positions
        # C'est en fait une simple distance de manhattan... lol
        x = pos2[0] - pos1[0]
        if x < 0:
            x = -x

        y = pos2[1] - pos1[1]
        if y < 0:
            y = -y

        delta = x+y
        # Si delta = 0, colision ! 
        return delta
    
    def check_player_ghost_collide(self, ghost:Ghost):
        p_pos = self.player.get_pos()
        g_pos = ghost.get_pos()
        dist = self.delta_pos(p_pos, g_pos)
        if dist == 0:
            if ghost.get_behavior():
                self.player.dead()
                if self.player.life <= 0:
                    self.state = "DEAD"
            else: # le fantome est mangeable
                ghost.state = "DEAD"
                ghost.anim1.kill()
                ghost.anim2.kill()
            return True
        return False
    
    def check_player_bonus_collide(self):
        i = 0
        while i < len(self.bonus):
            item = self.bonus[i]
            if isinstance(item, Item):
                b_pos = item.get_pos()
                p_pos = self.player.get_pos()
                if b_pos[0] == p_pos[0] and b_pos[1] == p_pos[1]:
                    self.score += 10
                    print(self.score)
                    item.get_bonus()
                    self.bonus_count -= 1
                    pos = self.player.get_pos()
                    self.fading_texts.append(Fading_Text(self.config, (pos[0]*32,pos[1]*32), "10", True))

                    if item.get_type_bonus() == 1:
                        ## Super bonus
                        self.chronos = Chrono(self.config, 8000)
                        for ghost in self.ghosts:
                            ghost.set_behavior(False)



                    break
            i += 1
        if i < len(self.bonus) and self.bonus[i].get_bonus() == 0:
            self.bonus.pop(i)
        

if __name__ == "__main__":

    G = Grid(Config())
    print(G.delta_pos([0,0],[0,0]))

    print(G.delta_pos([0,0],[-2,0]))
    print(G.delta_pos([0,0],[2,-2]))
    print(G.delta_pos([-2,-2],[2,2]))
    print(G.delta_pos([-2,2],[2,-2]))