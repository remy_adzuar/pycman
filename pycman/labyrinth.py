import pygame, random

from config import Config

class Labyrinth:

    def __init__(self, config:Config) -> None:
        self.config = config
        self.width = 39
        self.height = 22
        self.matrix = [[Case(0) for i in range(self.width)] for j in range(self.height)]
    
        self.floor_pos = []

    def set_case(self, pos, value):
        self.matrix[pos[1]][pos[0]] = value
    
    def get_case(self, pos):
        if pos[0] >= 0 and pos[0] < self.width:
            if pos[1] >= 0 and pos[1] < self.height:
                return self.matrix[pos[1]][pos[0]]
        return None

    def get_adjacent(self, pos):

        adjacent = {}
        if pos[0]-1 >= 0:
            adjacent["O"] = self.get_case((pos[0]-1,pos[1]))
        if pos[0]+1 < self.width:
            adjacent["E"] = self.get_case((pos[0]+1,pos[1]))
        if pos[1]-1 >= 0:
            adjacent["N"] = self.get_case((pos[0],pos[1]-1))
        if pos[1]+1 < self.height:
            adjacent["S"] = self.get_case((pos[0],pos[1]+1))
        return adjacent

    def random_labyrinth(self):
        y = 0
        while y < self.height:
            x = 0
            while x < self.width:
                rand = random.randrange(4)
                if rand > 0:
                    code = 0
                    self.floor_pos.append([x,y])
                else:
                    code = 1
                self.set_case((x,y), Case(code))
                x+= 1
            y += 1
    
    def load_from_file(self, URL):
        
        ## Lecture du fichier
        file = open(URL, "r")
        result = []
        for line in file:
            line_strip = line.strip()
            line_split = line_strip.split(",")
            line_split = [int(x) for x in line_split] ## Transformation en entier
            result.append(line_split)
        self.height = len(result)
        if self.height>0:
            self.width = len(result[0])
        
        ## Transposition info sous forme de case
        self.matrix = []
        y = 0
        while y < self.height:
            x = 0
            self.matrix.append([])
            while x < self.width:
                c = Case(result[y][x]) # y et x inverse c'est normal!
                self.matrix[y].append(c)
                if c.get_id() == 0:
                    self.floor_pos.append([x,y])
                x += 1
            y += 1
        
        self.set_wall_sprite()


    def draw_labyrinth(self):
        y = 0
        while y < self.height:
            x = 0
            while x < self.width:
                # pygame.draw.rect(self.config.get_screen(), (255,255,255), (x*32,y*32,32,32), 1)
                c = self.get_case((x,y))
                c.draw(self.config, (x,y))
                x += 1
            y += 1
    
    def give_random_floor_pos(self, number:int):
        if number<0 or number>len(self.floor_pos):
            number = len(self.floor_pos)
        ls_random = random.sample(range(0,len(self.floor_pos)),number)
        
        list_pos_floor = []
        for number in ls_random:
            list_pos_floor.append(self.floor_pos[number])
        return list_pos_floor
    
    def give_random_accessible_floor_pos(self, number:int):
        # Doit remplacer give_random_floor_pos pour ne renvoyer que
        # des positions réellement accessibles
        accessibles_floor_pos = self.get_list_floor_pos([0,14]) # 0,14 étant l'extrémité tunnel gauche
        if number < 0 or number>len(accessibles_floor_pos):
            number = len(accessibles_floor_pos)
        ls_random = random.sample(range(0,len(accessibles_floor_pos)), number)

        list_pos_floor = []
        for number in ls_random:
            list_pos_floor.append(accessibles_floor_pos[number])
        return list_pos_floor
    
    def get_neighbourg(self, pos):
        # Récupère les 8 voisins, 
        # None si pas de case,
        # Tous les index pair sont des case en diagonale
        # Tous les index impair sont des cases adjacente directement
        # comme suit : ["NO","N","NE","E","SE","S","SO","O"]
        result = []
        x = pos[0]
        y = pos[1]
        result.append(self.get_case((x-1,y-1))) ## TopLeft
        result.append(self.get_case((x,y-1))) ## TopMiddle
        result.append(self.get_case((x+1,y-1))) ## TopRight
        result.append(self.get_case((x+1,y))) ## MiddleRight 
        result.append(self.get_case((x+1,y+1))) ## BottomRight
        result.append(self.get_case((x,y+1))) ## BottomMiddle
        result.append(self.get_case((x-1,y+1))) ## BottomLeft 
        result.append(self.get_case((x-1,y))) ## MiddleLeft
        return result
    
    def count_neighbourg(self, pos):
        # Compte le nombre de voisin pour commencer a determiner la forme
        neighbourg = self.get_neighbourg(pos)
        cpt = 0
        i = 1
        while i < len(neighbourg):
            c = neighbourg[i]
            if c != None and c.get_id() == 1:
                cpt += 1
            i += 2
        return cpt
    
    def get_walls_neighbourg_pos(self, pos, id:int=1):
        # Retourne une liste de position pour les case de mur voisines
        neighbourg = self.get_neighbourg(pos)
        result = []
        x = pos[0]
        y = pos[1]

        if neighbourg[1] != None and neighbourg[1].get_id() == id:
            result.append([x,y-1])
        if neighbourg[3] != None and neighbourg[3].get_id() == id:
            result.append([x+1,y])
        if neighbourg[5] != None and neighbourg[5].get_id() == id:
            result.append([x,y+1])
        if neighbourg[7] != None and neighbourg[7].get_id() == id:
            result.append([x-1,y])
        return result

    def set_wall_sprite(self):
        # Ho boy ca sent l'usine à gaz...
        # Fonction qui doit déterminer le sprite a utiliser pour dessiner 
        # un mur et la rotation du dit sprite... Fichtre
        y = 0
        while y < self.height:
            x = 0
            while x < self.width:
                c = self.get_case((x,y))

                if c.get_id() == 1:
                    
                    nb_neighbourg = self.count_neighbourg((x,y))
                    neighbourg_pos = self.get_walls_neighbourg_pos((x,y))
                    if nb_neighbourg == 1:
                        # pas d'ambiguité ici
                        c.set_img("./imgs/walls/walls_end.png") # manque l'orientation
                        pos1 = neighbourg_pos[0]
                        if pos1[0] == x-1 and pos1[1] == y:
                            c.img = pygame.transform.rotate(c.img, 90)
                        elif pos1[0] == x and pos1[1] == y+1:
                            c.img = pygame.transform.rotate(c.img, 180)
                        elif pos1[0] == x+1 and pos1[1] == y:
                            c.img = pygame.transform.rotate(c.img, 270)
                    if nb_neighbourg == 3:
                        c.set_img("./imgs/walls/walls_Tee.png") # manque l'orientation
                        pos1 = neighbourg_pos[0]
                        pos2 = neighbourg_pos[1]
                        pos3 = neighbourg_pos[2]

                        # Fichtre!
                        if pos1[0] == pos3[0] and pos3[0] == x:
                            c.img = pygame.transform.rotate(c.img, 270)
                        elif pos1[1] == pos3[1] and pos2[1] == y+1:
                            c.img = pygame.transform.rotate(c.img, 180)
                        elif pos1[0] == pos2[0] and pos3[0] == x-1:
                            c.img = pygame.transform.rotate(c.img, 90)


                    if nb_neighbourg == 4:
                        # Pas d'ambiguité ici
                        c.set_img("./imgs/walls/walls_cross.png") # pas d'orientation

                    if nb_neighbourg == 2:
                        # Ambiguité entre coin et droit,
                        if len(neighbourg_pos) == 2:
                            pos1 = neighbourg_pos[0]
                            pos2 = neighbourg_pos[1]
                            if pos1[0] == pos2[0] or pos1[1] == pos2[1]:
                                # C'est une droite...
                                c.set_img("./imgs/walls/walls_straight.png") # manque l'orientation
                                if pos1[1] == pos2[1]:
                                    c.img = pygame.transform.rotate(c.img, 90)
                            else:
                                # C'est un coin
                                c.set_img("./imgs/walls/walls_corner.png")
                                if pos1[0] == x+1:
                                    c.img = pygame.transform.rotate(c.img, 270)
                                elif pos1[1] == y+1:
                                    c.img = pygame.transform.rotate(c.img, 180)
                                elif pos2[0] == x-1:
                                    c.img = pygame.transform.rotate(c.img, 90)
                x += 1
            y += 1
    
    def get_list_floor_pos(self, start_pos):
        # Donne la liste des case de type "floor", accessible depuis start_pos
        # Permet de mettre les bonus uniquement sur des cases accessibles
        # et du coup c'est mieux, notamment pour pouvoir finir le niveau
        set_open = [start_pos]
        set_close = []
        while len(set_open)>0:
            self.process_node(set_open, set_close)
        
        # set_close contient alors l'ensemble des cases accessibles depuis
        # start_pos
        return set_close

    def process_node(self, set_open, set_close):
        if len(set_open)>0:
            node = set_open.pop()
            list_floor = self.get_walls_neighbourg_pos(node, 0)
            for floor in list_floor:
                if floor not in set_close and floor not in set_open:
                    set_open.append(floor)
            
            set_close.append(node)

class Case:

    def __init__(self, id:int) -> None:
        self.id = id
        self.img = None
        if self.id == 1:
            self.type = "WALL"
            img = pygame.image.load("./imgs/walls/walls_solo.png")
            self.img = pygame.transform.scale(img, (32,32))
        elif self.id == 0:
            self.type = "FLOOR"
        elif self.id == 2:
            self.type = "GHOST_WALL"
            img = pygame.image.load("./imgs/walls/walls_limit.png")
            self.img = pygame.transform.scale(img, (32,32))

    def set_img(self, URL, rotate:int=0):
        img = pygame.image.load(URL)
        img = pygame.transform.rotate(img, rotate*90)
        img = pygame.transform.scale(img, (32,32))
        self.img = img

    def get_type(self):
        return self.type

    def get_id(self):
        return self.id

    def draw(self, config:Config, pos):
        size = config.get_tile_size()
        coords = (pos[0]*size, pos[1]*size, size, size)
        if self.id == 1:
            config.get_screen().blit(self.img, (coords[0], coords[1]))
            # pygame.draw.rect(config.get_screen(), (255,0,0), coords)
        elif self.id == 0:
            # pygame.draw.rect(config.get_screen(), (0,128,20), coords)
            pass
        elif self.id == 2:
            config.get_screen().blit(self.img, (coords[0], coords[1]))

if __name__ == "__main__":

    pygame.init()

    L = Labyrinth(Config())
    L.random_labyrinth()
    for line in L.matrix:
        for cas in line:
            print(cas.id, end="")
        print("")

    list_pos_floor = L.give_random_floor_pos(50)
    for pos in list_pos_floor:
        cas = L.get_case(pos)
        if cas.get_id() == 1:
            print(False)
        elif cas.get_id()==0:
            print("OK")
    print(len(L.give_random_floor_pos(5000))) # Louee soit la loi normale :3

    L.load_from_file("./levels/1.txt")
    print(L.get_neighbourg((0,0)))
    print(L.count_neighbourg((0,0)))
    for line in L.matrix:
        for c in line:
            print(c.get_id(), end="")
        print("")