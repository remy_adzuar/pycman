import pygame

class My_sprite(pygame.sprite.Sprite):

    
    def __init__(self, *groups) -> None:
        super().__init__(*groups)

        self.images = []

        self.index = 0
        self.frame_cpt = 0
        self.frame_cpt_limit = 8

        self.last_rotation = None

        self.image = None

        self.rect = pygame.Rect(0,0,16,16)
    
    def add_image(self, URL):
        img = pygame.image.load(URL)
        img = pygame.transform.scale(img, (32,32))
        self.images.append(img)
        self.image = self.images[self.index]

    def reset_rotate(self):
        if self.last_rotation != None:
            if self.last_rotation == 1:
                self.rotate_90(3)
            elif self.last_rotation == 2:
                self.rotate_90(2)
            elif self.last_rotation == 3:
                self.rotate_90(1)


    def rotate_90(self, nb:int=1):
        
        self.last_rotation = nb

        i = 0
        while i < len(self.images):
            self.images[i] = pygame.transform.rotate(self.images[i], 90*nb)
            i += 1
    
    
    def action_rotate(self, nb:int=1):
        if self.last_rotation != None or self.last_rotation != 0:
            self.reset_rotate()
        self.last_rotation = nb
        self.rotate_90(nb)

    def set_pos(self, pos):
        self.rect.x = pos[0]
        self.rect.y = pos[1]
    
    def set_frame_limit(self, nb:int):
        # Change le nombre de frame à passer avant de passer à l'image suivante
        self.frame_cpt_limit = nb

    def update(self):

        self.frame_cpt += 1
        if self.frame_cpt >= self.frame_cpt_limit:
            self.index += 1
            self.frame_cpt = 0

        if self.index >= len(self.images):
            self.index = 0

        self.image = self.images[self.index]
    

if __name__ == "__main__":

    my_group = pygame.sprite.Group()
    Sprite = My_sprite(my_group)

    Sprite.add_image("./imgs/player/PycMan-4.png")
    Sprite.add_image("./imgs/player/PycMan-3.png")
    Sprite.add_image("./imgs/player/PycMan-2.png")
    Sprite.add_image("./imgs/player/PycMan-1.png")

    Sprite2 = My_sprite(my_group)
    Sprite2.add_image("./imgs/player/PycMan-1.png")
    Sprite2.add_image("./imgs/player/PycMan-2.png")
    Sprite2.add_image("./imgs/player/PycMan-3.png")
    Sprite2.add_image("./imgs/player/PycMan-4.png")
    Sprite2.set_pos((128,128))


    pygame.init()
    screen = pygame.display.set_mode((256,256))
    clock = pygame.time.Clock()

    # Sprite.rotate_90(2)
    # Sprite.reset_orientation()
    # Sprite.rotate_90(1)
    Sprite2.rotate_90(1)

    while True:
        screen.fill((0,0,0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    Sprite.reset_rotate() # Actionner le reset
                    # Alors que le sprite est a 180 degres permet d'inverser les commandes
                    # Serendipity
                elif event.key == pygame.K_LEFT:
                    Sprite.action_rotate(2)
                elif event.key == pygame.K_RIGHT:
                    Sprite.action_rotate(0)
                elif event.key == pygame.K_UP:
                    Sprite.action_rotate(1)
                elif event.key == pygame.K_DOWN:
                    Sprite.action_rotate(3)
        
        Sprite.update()
        Sprite2.update()
        my_group.draw(screen)

        pygame.display.update()
        clock.tick(60)