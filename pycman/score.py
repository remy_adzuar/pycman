class Score:

    def __init__(self) -> None:
        self.scores = []
    
    def read_file_score(self, URL):
        try:
            file = open(URL, "r")
        except:
            file = open(URL, "x")
            file.close()
            file = open(URL, "r")

        self.scores = []
        for line in file:
            strip = line.strip()
            split = strip.split(",")
            try:
                int(split[1])
            except:
                print("ERREUR FICHIER scores.txt")
                return None
            
            self.scores.append(split[0:2])
        file.close()

    def get_scores(self):
        return self.scores
    
    def sort_scores(self):
        self.scores.sort(key=
        lambda x:int(x[1]), reverse=True)
    
    def get_min_score(self):
        if len(self.scores)>0:
            self.sort_scores()
            return self.scores[-1]
    
    def append_score(self,score):
        try:
            int(score[1])
            str(score[0])
        except:
            print("ERREUR SCORE À ENREGISTRER")
            return None
        
        if len(self.scores)< 10:
            self.scores.append(score)
        else:
            mini = self.get_min_score()
            if mini[1] < score[1]:
                self.scores.pop(-1)
                self.scores.append(score)
        self.sort_scores()

    def write_score_file(self,URL):
        file = open(URL,"w")
        self.sort_scores()
        for line in self.scores:
            s = str(line[0]+","+str(line[1])+"\n")
            file.write(s)
        file.close()

if __name__ == "__main__":

    S = Score()
    S.read_file_score("./score_test.txt")
    print(S.scores)

    S.append_score(["TEST",1250])
    S.write_score_file("./score_test.txt")